import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import { store } from "./app/redux/store";
import ImagesList from "./app/containers/ImagesList";
import FullImage from "./app/components/FullImage";

const Stack = createStackNavigator();

function App(): JSX.Element {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Gallery">
                    <Stack.Screen name="Gallery" component={ImagesList}/>
                    <Stack.Screen name="image" component={FullImage} />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
}

export default App;
