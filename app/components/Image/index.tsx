import React, { FunctionComponent } from 'react';
import { useDispatch } from 'react-redux';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { selectImagesRoutine } from "../../redux/router";
import { useNavigation } from '@react-navigation/native';

interface IProps {
    description: string,
    urlFull: string,
    AuthorName: string
}

const styles = StyleSheet.create({
    wrapper: {
        display: "flex",
        flexDirection: "row",
        position: "relative",
        flex: 0.3,
        backgroundColor: "red",
        borderWidth: 51,
    },
    description: {
        display: "flex",
        position: "absolute",
        opacity: 0.8,
        bottom: 20,
        backgroundColor: "white"
    },
    tinyLogo: {
        width: '100%',
        height: 250,
        marginBottom: 20
    },

});

const ImageBlock: FunctionComponent<IProps> = ({ description, urlFull, AuthorName}) => {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const selectImage = () => {
        dispatch(selectImagesRoutine.trigger({ description, urlFull, AuthorName}));
        navigation.navigate('image');
    }
    return (
        <TouchableOpacity onPress={selectImage}>
            <View>
                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: urlFull
                    }}
                />
                <View style={styles.description}>
                    <Text>Author: {AuthorName}</Text>
                    <Text>Description: {description}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default ImageBlock;