import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { FunctionComponent } from "react";
import { useSelector } from "react-redux";
import { IRootState } from "../../common/models/RootState";

const styles = StyleSheet.create({
    wrapper: {
        display: "flex",
        flexDirection: "row",
        position: "relative",
        flex: 0.3,
        backgroundColor: "red",
        borderWidth: 51,
    },
    description: {
        display: "flex",
        position: "absolute",
        opacity: 0.8,
        bottom: 20,
        backgroundColor: "white"
    },
    tinyLogo: {
        width: '100%',
        height: '100%'
    },

});

const FullImage: FunctionComponent = () => {
    const selectImg = useSelector((state: IRootState) => state.img.selectImage);
    return (
        <TouchableOpacity>
            <View>
                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: selectImg.urlFull
                    }}
                />
                <View style={styles.description}>
                    <Text>Author: {selectImg.AuthorName}</Text>
                    <Text>Description: {selectImg.description}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default FullImage;