import { ApiService } from './AbstractService';
import { image } from '../common/models/responseImages';
import axios, { AxiosInstance } from 'axios';

class ImagesService extends ApiService {
    constructor(private app: AxiosInstance) {
        super();
    }
    getImages = async (): Promise<Array<image>> => {
        const res = await this.app.get(`https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0`);

        return res.data;
    };
}

class Api {
    private readonly app: AxiosInstance;

    public images: ImagesService;

    constructor() {
        this.app = axios.create();
        this.images = new ImagesService(this.app)
    }
}

export const API = new Api();