import { IImagesState } from '../../redux/reducers';

export interface IRootState {
    img: IImagesState
}