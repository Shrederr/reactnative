export interface image {
    id:string,
    alt_description:string,
    urls: {
        full:string,
        small:string
    },
    user: {
        name: string
    }
}