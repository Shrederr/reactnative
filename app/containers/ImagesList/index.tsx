import React, { FunctionComponent, useEffect } from 'react';
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../common/models/RootState";
import { fetchImagesRoutine } from "../../redux/router";
import ImageBlock from "../../components/Image";

const styles = StyleSheet.create({
    main:{
        backgroundColor:"black"
    }
});

const ImagesList: FunctionComponent = () => {
    const dispatch = useDispatch();
    const img = useSelector((state: IRootState) => state.img);
    useEffect(() => {
        dispatch(fetchImagesRoutine.trigger());
    }, []);
    return (
        <SafeAreaView>
            <ScrollView style={styles.main}>
                {
                    img.images.length > 0 ?
                        img.images.map(image => (
                            <ImageBlock
                                key={image.id}
                                description={image.alt_description}
                                AuthorName={image.user.name}
                                urlFull={image.urls.full}
                            />)
                               ) : null
                 }
                            </ScrollView>
                    </SafeAreaView>
                    )};

export default ImagesList;