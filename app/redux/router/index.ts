import { createRoutine } from 'redux-saga-routines';

export const fetchImagesRoutine = createRoutine('FETCH_IMAGES');
export const selectImagesRoutine = createRoutine('SELECT_IMAGE');