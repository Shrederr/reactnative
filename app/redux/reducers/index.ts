import { Routine } from 'redux-saga-routines';
import {fetchImagesRoutine, selectImagesRoutine} from "../router";
import { image } from "../../common/models/responseImages";

export interface IImagesState {
    images: Array<image>;
    selectImage: {
        description: string,
        urlFull: string,
        AuthorName: string
    }
}

const initialState: IImagesState = {
    images: [],
    selectImage: {
        description: 'string',
        urlFull: 'string',
        AuthorName: 'string'
    }
};

export const imageReducer = (state = initialState, action: Routine<any>): IImagesState => {
    switch (action.type) {
        case fetchImagesRoutine.SUCCESS:
            return {
                ...state,
                images: [...action.payload.images]
            };
        case selectImagesRoutine.TRIGGER:
            const { description, urlFull, AuthorName } = action.payload;
            return {
                ...state,
                selectImage:{
                    description,
                    urlFull,
                    AuthorName
                }
            }
        default:
            return state;
    }
};