import { all } from 'redux-saga/effects';
import imagesSaga from './saga';

export default function* rootSaga() {
    yield all([
        imagesSaga()
    ]);
}