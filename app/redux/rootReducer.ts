import { combineReducers } from 'redux';
import { imageReducer } from './reducers';

const RootReducer = combineReducers({
    img: imageReducer
});

export type RootState = ReturnType<typeof RootReducer>;

export default RootReducer;