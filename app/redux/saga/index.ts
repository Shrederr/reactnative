import { call, put, all, takeEvery } from 'redux-saga/effects';
import { fetchImagesRoutine } from '../router';
import { API } from '../../API/imagesService';

function* fetchImages() {
    try {
        const data = yield call(API.images.getImages);
        yield put(fetchImagesRoutine.success({images: data}));
    } catch (e) {
        yield put(fetchImagesRoutine.failure(e.message));
    }
}

function* watchQueryRequest() {
    yield takeEvery(fetchImagesRoutine.TRIGGER, fetchImages);
}

export default function* imagesSaga() {
    yield all([
        watchQueryRequest()
    ]);
}